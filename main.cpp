/*
 * Main.cpp
 *
 *  Created on: Fall 2021
 */
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include <CImg.h>
using namespace std;
using namespace cimg_library;

// Data type for image components
// FIXME: Change this type according to your group assignment
typedef double data_t;

const uint cpu = 8;	//Numero de CPU 

const char* SOURCE_IMG      = "bailarina.bmp";
const char* SOURCE_IMG_NEGRO= "background_V.bmp";
const char* DESTINATION_IMG = "bailarina2.bmp";
void *imagen(void *elem);

CImg<data_t> srcImage(SOURCE_IMG);

	data_t *pRsrcnegro, *pGsrcnegro, *pBsrcnegro; // Pointers to the R, G and B components
	data_t *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components
	data_t *pRdest, *pGdest, *pBdest;
	data_t *pDstImage; // Pointer to the new image pixels
	uint width, height; // Width and height of the image
	uint widthnegro, heightnegro; // Width and height of the image
	uint nComp; // Number of image components
	uint cont;
	uint repeticiones = 15;
	
void *imagen(void *elem){
	cont = *((uint *) elem);
	
	uint j = ((width * height)/cpu) * cont;
	uint size = ((width * height)/cpu) + j;
	if(cpu -1 == cont){
		size = width * height;
	}
	for(uint k=0; k< repeticiones; k++){
		for (uint i = j; i < size; i++){
			*(pRdest + i) = 255.0-(((255.0-(*(pRsrcnegro+i)))*256.0)/(*(pRsrc+i)+1.0));
			if(*(pRdest + i) < 0){
				*(pRdest + i) = 0;
			}
			*(pGdest + i) = 255.0-(((255.0-(*(pGsrcnegro+i)))*256.0)/(*(pGsrc+i)+1.0));
			if(*(pGdest + i) < 0){
				*(pGdest + i) = 0;
			}
			*(pBdest + i) = 255.0-(((255.0-(*(pBsrcnegro+i)))*256.0)/(*(pBsrc+i)+1.0));
			if(*(pBdest + i) < 0){
				*(pBdest + i) = 0;
			}
		}
	}

	return 0;
	}

int main() {

	 CImg<data_t> srcImage(SOURCE_IMG_NEGRO);
    
    srcImage.display(); // Displays the source image
	widthnegro  = srcImage.width(); // Getting information from the source image
	heightnegro = srcImage.height();

    pRsrcnegro = srcImage.data(); // pRcomp points to the R component array
	pGsrcnegro = pRsrcnegro + heightnegro * widthnegro; // pGcomp points to the G component array
	pBsrcnegro = pGsrcnegro + heightnegro * widthnegro; // pBcomp points to B component array

	// Open file and object initialization
	CImg<data_t> srcImage2(SOURCE_IMG);


	/***************************************************
	 * TODO: Variables initialization.
	 *   - Prepare variables for the algorithm
	 *   - This is not included in the benchmark time
	 */
	struct timespec tStart, tEnd;
	double dElapsedTimeS;

	srcImage2.display(); // Displays the source image
	width  = srcImage2.width(); // Getting information from the source image
	height = srcImage2.height();
	nComp  = srcImage2.spectrum(); // source image number of components
				// Common values for spectrum (number of image components):
				//  B&W images = 1
				//	Normal color images = 3 (RGB)
				//  Special color images = 4 (RGB and alpha/transparency channel)


	// Allocate memory space for destination image components
	pDstImage = (data_t *) malloc (width * height * nComp * sizeof(data_t));
	if (pDstImage == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}

	// Pointers to the componet arrays of the source image
	pRsrc = srcImage2.data(); // pRcomp points to the R component array
	pGsrc = pRsrc + height * width; // pGcomp points to the G component array
	pBsrc = pGsrc + height * width; // pBcomp points to B component array

	// Pointers to the RGB arrays of the destination image
	pRdest = pDstImage;
	pGdest = pRdest + height * width;
	pBdest = pGdest + height * width;
	unsigned short i;


	
	/***********************************************
	 * TODO: Algorithm start.
	 *   - Measure initial time
	 */
	if(clock_gettime(CLOCK_REALTIME, &tStart) == -1){
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}

	/************************************************
	 * FIXME: Algorithm.
	 * In this example, the algorithm is a components swap
	 *
	 * TO BE REPLACED BY YOUR ALGORITHM
	 * */
	pthread_t Threads[cpu];
	int A[cpu];
	
		for (i = 0; i < cpu; i++){
			Threads[i]=i;
			A[i]=i;
			int pthread_ret = pthread_create(&Threads[i], NULL, imagen, &A[i]);
			if (pthread_ret!=0)
		{
			printf("ERROR: pthread_create error code: %d.\n", pthread_ret);
			exit(EXIT_FAILURE);
		}
	}
		for (i = 0; i < cpu; i++)
			pthread_join(Threads[i], NULL);
	

	/***********************************************
	 * TODO: End of the algorithm.
	 *   - Measure the end time
	 *   - Calculate the elapsed time
	 */
	if(clock_gettime(CLOCK_REALTIME, &tEnd) == -1){
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}
	dElapsedTimeS = (tEnd.tv_sec - tStart.tv_sec);
	dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;

		
	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.
	CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);

	// Store destination image in disk
	dstImage.save(DESTINATION_IMG); 

	// Display destination image
	dstImage.display();

	cout << "Tiempo final: " << dElapsedTimeS ;
	return 0;
}
